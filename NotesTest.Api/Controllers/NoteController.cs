﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotesTest.BLL.Models;
using NotesTest.BLL.Services.Interfaces;

namespace NotesTest.Api.Controllers
{
    [Route("[controller]/[action]")]
    public class NoteController : Controller
    {
        private readonly INoteService _noteService;
        public NoteController(INoteService noteService)
        {
            _noteService = noteService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateRequest requestModel)
        {
            var responseModel = await _noteService.Create(requestModel);
            return Json(responseModel);
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateRequest requestModel)
        {
            var responseModel = await _noteService.Update(requestModel);
            return Json(responseModel);
        }

        [HttpPost]
        public async Task<IActionResult> Delete([FromBody] DeleteRequest requestModel)
        {
            var responseModel = await _noteService.Delete(requestModel);
            return Json(responseModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var responseModel = await _noteService.GetAll();
            return Ok(responseModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(string id)
        {
            var responseModel = await _noteService.GetById(id);
            return Ok(responseModel);
        }
    }
}