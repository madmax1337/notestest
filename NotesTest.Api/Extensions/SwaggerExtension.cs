﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace NotesTest.Api.Extensions
{
    public class SwaggerExtension
    {
        public static void Add(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {Title = "NoteTest-API", Version = "v1"});
            });
        }
    }
}
