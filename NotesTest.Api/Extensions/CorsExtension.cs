﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesTest.Api.Extensions
{
    public class CorsExtension
    {
        public static void Add(IServiceCollection services, IConfiguration configuration)
        {
            var corsOptions = configuration.GetSection("Cors");
            var origins = corsOptions["Origins"];
            services.AddCors(options =>
            {
                options.AddPolicy("AllPolicy", b => b.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowCredentials());

                options.AddPolicy("OriginPolicy", b => b.WithOrigins(origins)
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials());
            });
        }
    }
}
