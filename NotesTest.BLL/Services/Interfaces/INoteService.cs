﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NotesTest.BLL.Models;

namespace NotesTest.BLL.Services.Interfaces
{
    public interface INoteService
    {
        Task<string> Create(CreateRequest requestModel);
        Task<string> Update(UpdateRequest requestModel);
        Task<string> Delete(DeleteRequest requestModel);
        Task<GetAllResponse> GetAll();
        Task<GetByIdResponse> GetById(string id);
    }
}
