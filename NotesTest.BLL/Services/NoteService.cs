﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using NotesTest.BLL.Models;
using NotesTest.BLL.Services.Interfaces;
using NotesTest.DAL.Entity;
using NotesTest.DAL.Repository.Interface;

namespace NotesTest.BLL.Services
{
    public class NoteService : INoteService
    {
        private readonly INoteRepository _noteRepository;
        private readonly IMapper _mapper;

        public NoteService(INoteRepository noteRepository, IMapper mapper)
        {
            _noteRepository = noteRepository;
            _mapper = mapper;
        }

        public async Task<string> Create(CreateRequest requestModel)
        {
            Note entity = _mapper.Map<CreateRequest, Note>(requestModel);
            var queryResult = await _noteRepository.Create(entity);
            return (queryResult);
        }

        public async Task<string> Delete(DeleteRequest requestModel)
        {
            var queryResult = await _noteRepository.Delete(requestModel.Id);
            return queryResult;
        }

        public async Task<GetAllResponse> GetAll()
        {
            var queryResult = await _noteRepository.GetAll();
            GetAllResponse responnseModel = new GetAllResponse();
            responnseModel.Items = _mapper.Map<List<Note>, List<GetAllItem>>(queryResult);
            return responnseModel;
        }

        public async Task<GetByIdResponse> GetById(string id)
        {
            var queryResult = await _noteRepository.GetById(id);
            GetByIdResponse responseModel = new GetByIdResponse();
            responseModel = _mapper.Map<Note, GetByIdResponse>(queryResult);

            return responseModel;
        }

        public async Task<string> Update(UpdateRequest requestModel)
        {
            var items = await _noteRepository.GetById(requestModel.Id);

            var entity = CompletitionModel(requestModel, items.CreationDate);

            var queryResult = await _noteRepository.Update(entity);

            return (queryResult);
        }

        private Note CompletitionModel(UpdateRequest requestModel, DateTime creatiomDate)
        {
            Note entity = new Note();
            entity.Id = requestModel.Id;
            entity.CreationDate = creatiomDate;
            entity.Description = requestModel.Description;
            entity.DateUpdated = DateTime.Now;
            entity.Name = requestModel.Name;
            entity.UserName = requestModel.UserName;
            return entity;
        }
    }
}
