﻿using System;

namespace NotesTest.BLL.Models
{
    public class GetByIdResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
