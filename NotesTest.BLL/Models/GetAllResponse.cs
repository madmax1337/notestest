﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotesTest.BLL.Models
{
    public class GetAllResponse
    {
        public List<GetAllItem> Items { get; set; }

        public GetAllResponse()
        {
            Items = new List<GetAllItem>();
        }
    }

    public class GetAllItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
