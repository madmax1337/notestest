﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotesTest.BLL.Models
{
    public class CreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
