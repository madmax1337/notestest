﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotesTest.BLL.Models
{
    public class GetByIdRequest
    {
        public string Id { get; set; }
    }
}
