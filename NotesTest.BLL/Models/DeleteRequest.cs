﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotesTest.BLL.Models
{
    public class DeleteRequest
    {
        public string Id { get; set; }
    }
}
