﻿using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NotesTest.BLL.Automaper;
using NotesTest.BLL.Services;
using NotesTest.BLL.Services.Interfaces;

namespace NotesTest.BLL
{
    public class Config
    {
        public static void Add(IServiceCollection services)
        {
            DAL.Config.Add(services);
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new NoteMapper());
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddTransient<INoteService, NoteService>();
        }
    }
}
