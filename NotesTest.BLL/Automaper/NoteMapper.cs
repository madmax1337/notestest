﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using NotesTest.BLL.Models;
using NotesTest.DAL.Entity;

namespace NotesTest.BLL.Automaper
{
    public class NoteMapper: Profile
    {
        public NoteMapper()
        {
            CreateMap<CreateRequest, Note>();
            CreateMap<UpdateRequest, Note>();
            CreateMap<Note, GetAllItem>();
            CreateMap<Note, GetByIdResponse>();
        }
    }
}
