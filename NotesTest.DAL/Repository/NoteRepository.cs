﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using NotesTest.DAL.Entity;
using NotesTest.DAL.Repository.Interface;

namespace NotesTest.DAL.Repository
{
    public class NoteRepository : INoteRepository
    {
        private readonly IMemoryCache _memoryCache;
        public NoteRepository(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
            SeedData();
        }

        private void SeedData()
        {
            if (_memoryCache.Get<List<Note>>("notesData") == null)
            {
                List<Note> dataContainer = new List<Note>();
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "asdsdfsdafgsdfgdsfgfsdag",
                    Name = "Label",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "fdsfsgdsfghcv bgvchdfghdfhgd",
                    Name = "Test",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "asdgsdfgdfgdfgdfgdfgdfgfg",
                    Name = "Test",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "gfdgbncmkdsfsdfsdfsdfdsafasdfsadf",
                    Name = "Test2",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "asdsdfsdafgsdfgdsfgfsdag",
                    Name = "Label",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "asdsdfsdafgsdfgdsfgfsdag",
                    Name = "Label",
                    UserName = "Max"
                });
                dataContainer.Add(new Note()
                {
                    CreationDate = DateTime.Now,
                    DateUpdated = DateTime.Now,
                    Description = "asdsdfsdafgsdfgdsfgfsdag",
                    Name = "Label",
                    UserName = "Max"
                });

                _memoryCache.Set("notesData", dataContainer, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(360)
                });
            }
        }

        public async Task<string> Create(Note entity)
        {
            var notes = _memoryCache.Get<List<Note>>("notesData");

            notes.Add(entity);

            _memoryCache.Set("notesData", notes);
            return entity.Id;
        }

        public async Task<string> Delete(string id)
        {
            var notes = _memoryCache.Get<List<Note>>("notesData");
            var queryResult = notes.Where(note => note.Id != id).ToList();
            _memoryCache.Set("notesData", queryResult);
            return id;
        }

        public async Task<List<Note>> GetAll()
        {
            var notes = _memoryCache.Get<List<Note>>("notesData");
            return notes;
        }

        public async Task<Note> GetById(string id)
        {
            var notes = _memoryCache.Get<List<Note>>("notesData");
            return notes.Where(note => note.Id == id).FirstOrDefault();
        }

        public async Task<string> Update(Note entity)
        {
            var notes = _memoryCache.Get<List<Note>>("notesData");
            var queryResult = notes.Where(note => note.Id != entity.Id).ToList();
             queryResult.Add(entity);
             _memoryCache.Set("notesData", queryResult);

            return entity.Id;
        }
    }
}
