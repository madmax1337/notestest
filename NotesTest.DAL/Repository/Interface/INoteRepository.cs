﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NotesTest.DAL.Entity;

namespace NotesTest.DAL.Repository.Interface
{
    public interface INoteRepository
    {
        Task<string> Create(Note entity);
        Task<string> Update(Note entity);
        Task<string> Delete(string id);
        Task<List<Note>> GetAll();
        Task<Note> GetById(string id);
    }
}
