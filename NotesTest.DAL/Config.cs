﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using NotesTest.DAL.Repository;
using NotesTest.DAL.Repository.Interface;

namespace NotesTest.DAL
{
    public class Config
    {
        public static void Add(IServiceCollection services)
        {
            services.AddScoped<INoteRepository, NoteRepository>();
        }
    }
}
