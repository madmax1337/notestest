﻿using System;

namespace NotesTest.DAL.Entity
{
    public class Note
    {
        public Note()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.Now;
        }

        public string Id { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime DateUpdated { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
