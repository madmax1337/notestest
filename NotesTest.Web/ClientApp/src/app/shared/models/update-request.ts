export class UpdateRequest {
  id: string;
  name: string;
  description: string;
  userName: string;
}
