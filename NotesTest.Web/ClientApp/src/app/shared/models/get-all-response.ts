export class GetAllResponse {
  items: Array<GetAllItem>;
  constructor() {
    this.items = new Array<GetAllItem>();
  }
}

export class GetAllItem {
  id: string;
  name: string;
  description: string;
  userName: string;
}
