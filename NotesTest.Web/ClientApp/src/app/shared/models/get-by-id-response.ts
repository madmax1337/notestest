export class GetByIdResponse {
  id: string;
  name: string;
  description: string;
  userName: string;
}
