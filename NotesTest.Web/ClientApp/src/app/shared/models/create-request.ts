export class CreateRequest {
  name: string;
  description: string;
  userName: string;
}
