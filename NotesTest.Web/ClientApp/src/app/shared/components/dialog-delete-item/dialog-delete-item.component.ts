import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-delete-item',
  templateUrl: './dialog-delete-item.component.html',
  styleUrls: ['./dialog-delete-item.component.scss']
})
export class DialogDeleteItemComponent {

  constructor(public dialogRef: MatDialogRef<DialogDeleteItemComponent>) {
  }

  deleteItem(): void {
    this.dialogRef.close(true);
  }

  closeWindow(): void {
    this.dialogRef.close();
  }
}
