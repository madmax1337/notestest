import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';

import {
  CreateRequest,
  DeleteRequest,
  GetAllItem,
  GetAllResponse,
  GetByIdResponse,
  UpdateRequest
} from '../shared/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private readonly apiControllerName: string = 'Note';
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl + this.apiControllerName;
  }

  create(requestModel: CreateRequest): Observable<string> {
    return this.http.post<string>(this.apiUrl + '/Create', requestModel);
  }

  update(requestModel: UpdateRequest): Observable<string> {
    return this.http.put<string>(this.apiUrl + '/Update', requestModel);
  }

  delete(requestModel: DeleteRequest): Observable<string> {
    return this.http.post<string>(this.apiUrl + '/Delete', requestModel);
  }

  getAll(): Observable<GetAllResponse> {
    return this.http.get<GetAllResponse>(this.apiUrl + '/GetAll');
  }


  getById(id: string): Observable<GetByIdResponse> {
    return this.http.get<GetByIdResponse>(this.apiUrl + '/GetById?id=' + id);
  }
}
