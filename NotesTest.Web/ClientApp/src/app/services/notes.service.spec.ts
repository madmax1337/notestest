import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CreateRequest } from '../shared/models';
import { DeleteRequest } from '../shared/models';
import { UpdateRequest } from '../shared/models';
import { NotesService } from './notes.service';

describe('NotesService', () => {
  let service: NotesService;
  beforeEach(() => {
    const createRequestStub = () => ({

    });
    const deleteRequestStub = () => ({

    });
    const updateRequestStub = () => ({

    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        NotesService,
        { provide: CreateRequest, useFactory: createRequestStub },
        { provide: DeleteRequest, useFactory: deleteRequestStub },
        { provide: UpdateRequest, useFactory: updateRequestStub }
      ]
    });
    service = TestBed.get(NotesService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  //
  describe('create', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.get(HttpTestingController);
      const createRequestStub: CreateRequest = TestBed.get(CreateRequest);
      service.create(createRequestStub).subscribe(res => {
        console.log(res);
        
        //expect(res).toEqual(createRequestStub);
      });
      const req = httpTestingController.expectOne('http://localhost:56423/Note/Create');
      expect(req.request.method).toEqual('POST');
      req.flush(createRequestStub);
      httpTestingController.verify();
    });
  });

  describe('update', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.get(HttpTestingController);
      const updateRequestStub: UpdateRequest = TestBed.get(UpdateRequest);
      service.update(updateRequestStub).subscribe(res => {
        //expect(res).toEqual(updateRequestStub);
      });
      const req = httpTestingController.expectOne('http://localhost:56423/Note/Update');
      expect(req.request.method).toEqual('PUT');
      req.flush(updateRequestStub);
      httpTestingController.verify();
    });
  });
  describe('delete', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.get(HttpTestingController);
      const deleteRequestStub: DeleteRequest = TestBed.get(DeleteRequest);
      service.delete(deleteRequestStub).subscribe(res => {
        //        expect(res).toEqual(deleteRequestStub);
      });
      const req = httpTestingController.expectOne('http://localhost:56423/Note/Delete');
      expect(req.request.method).toEqual('POST');
      req.flush(deleteRequestStub);
      httpTestingController.verify();
    });
  });

  describe('getAll', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.get(HttpTestingController);
      let response: number;
      service.getAll().subscribe(res => {
        response = res.items.length;
        //expect(res.items.length).toEqual(0, 'should have empty heroes array');
      });
      const req = httpTestingController.expectOne('http://localhost:56423/Note/GetAll');
      //expect(req.request.method).toEqual('GET');
      req.flush(response);
      httpTestingController.verify();
    });
  });
});
