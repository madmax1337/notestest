import {Component, OnInit} from '@angular/core';
import {CreateRequest, DeleteRequest, GetAllItem, GetAllResponse, UpdateRequest} from '../shared/models';
import {MatTableDataSource} from '@angular/material/table';
import {NotesService} from '../services/notes.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material/dialog';
import {DialogNotesComponent} from './dialog-notes/dialog-notes.component';
import * as _ from "lodash";
import {DialogDeleteItemComponent} from '../shared/components/dialog-delete-item/dialog-delete-item.component';
import {DialogInfornationComponent} from './dialog-infornation/dialog-infornation.component';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  dataSource: MatTableDataSource<GetAllItem>;
  displayedColumns: string[] = ['name', 'description', 'userName', 'crud'];

  constructor(private notesService: NotesService,
              private toastr: ToastrService,
              public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource<GetAllItem>();
  }

  ngOnInit() {
    this.getNotes();
  }

  getNotes(): void {
    this.notesService.getAll().subscribe((data: GetAllResponse) => {

      if (data.items.length) {
        this.dataSource.data = data.items;
        return;
      }
      if (!data.items.length) {
        this.dataSource = new MatTableDataSource<GetAllItem>();
      }
    });
  }

  createNotes(): void {
    const dialogRef = this.dialog.open(DialogNotesComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: GetAllItem) => {
      if (result != null) {
        let model: CreateRequest = new CreateRequest();
        model = _.cloneDeep(result);
        this.notesService.create(model).subscribe((data: string) => {
          this.toastr.success("Add new notes");
          this.getNotes();
        });
      }
    });
  }

  updateNotes(element: GetAllItem): void {
    const dialogRef = this.dialog.open(DialogNotesComponent, {
      data: element,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: GetAllItem) => {
      if (result != null) {

        let model: UpdateRequest = new UpdateRequest();
        model = _.cloneDeep(result);
        model.id = element.id;

        this.notesService.update(model).subscribe((data: string) => {

          this.toastr.success("Update notes");
          this.getNotes();
        });
      }
    });
  }

  deleteNotes(element: GetAllItem): void {
    const dialogRef = this.dialog.open(DialogDeleteItemComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        let model: DeleteRequest = new DeleteRequest();
        model.id = element.id;

        this.notesService.delete(model).subscribe((data: string) => {

          this.toastr.success("Delete note");
          this.getNotes();
        });
      }
    });
  }

  getInformation(id: string) {
    const dialogRef = this.dialog.open(DialogInfornationComponent, {
      data: id,
      disableClose: true
    });
  }
}
