import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetAllItem } from 'src/app/shared/models';
import * as _ from "lodash"

@Component({
  selector: 'app-dialog-notes',
  templateUrl: './dialog-notes.component.html',
  styleUrls: ['./dialog-notes.component.scss']
})
export class DialogNotesComponent {

  notesForm: FormGroup;
  isUpdate: boolean;
  name: string;
  description: string;

  constructor(private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogNotesComponent>,
    @Inject(MAT_DIALOG_DATA) public noteItem: GetAllItem) {
    this.formGroup();
    this.inizializationDate();
  }

  private formGroup(): void {
    this.notesForm = this.formBuilder.group({
      userName: new FormControl(null, [Validators.required]),
      name: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
    });
  }

  private inizializationDate(): void {
    if (this.noteItem !== null) {
      this.isUpdate = true;
      this.notesForm.patchValue(_.cloneDeep(this.noteItem));
      this.name = this.noteItem.name;
      this.description = this.noteItem.description;
    }

    if (this.noteItem === null) {
      this.noteItem = new GetAllItem();
      this.isUpdate = false;
    }
  }

  closeWindow(): void {
    this.dialogRef.close();
  }

  submit(): void {
    this.noteItem = _.cloneDeep(this.notesForm.value)
    this.noteItem.name = this.name;
    this.noteItem.description = this.description;
    this.dialogRef.close(this.noteItem);
  }
}
