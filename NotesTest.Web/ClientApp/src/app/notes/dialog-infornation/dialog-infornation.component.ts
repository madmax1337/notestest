import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GetAllItem, GetByIdResponse } from 'src/app/shared/models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotesService } from 'src/app/services/notes.service';
import * as _ from "lodash";

@Component({
  selector: 'app-dialog-infornation',
  templateUrl: './dialog-infornation.component.html',
  styleUrls: ['./dialog-infornation.component.scss']
})
export class DialogInfornationComponent {

  notesForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private notesService: NotesService,
    public dialogRef: MatDialogRef<DialogInfornationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) {
    this.formGroup();
    this.inizializationDate();
  }

  private formGroup(): void {
    this.notesForm = this.formBuilder.group({
      userName: new FormControl(null),
      name: new FormControl(null),
      description: new FormControl(null),
    });
  }

  private inizializationDate(): void {
    this.notesService.getById(this.data).subscribe((data: GetByIdResponse) => {
      if (data) {
        this.notesForm.patchValue(_.cloneDeep(data));
      }
    });
  }

  closeWindow(): void {
    this.dialogRef.close();
  }
}
